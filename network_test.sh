#!/bin/bash

#H
#H  network_test.sh
#H
#H  DESCRIPTION
#H    This script will make a test and diagnostics for network interfaces. it
#H    use the iperf v2 test utility so it requires an iperf server to be setup
#H    on the other end. It will look not only to the bandwidth but also to the
#H    CPU utilization and interrupts, errors and setup. It will always test the
#H    interface in the default route.
#H
#H  USAGE
#H    script_name [-h] [-d] [-u] [-b] [-t] -c iperf_server
#H
#H  ARGUMENTS
#H    -c           iperf server (has to be setup in the same mode as the test
#H                 TCP or UDP)
#H    -u           Optional.Perform UDP Tests (default TCP Tests)
#H    -b           Optional.Perform Simultaneous Bidirectional Tests (default
#H                 alternate bidirectional TX/RX)
#H    -t           Optional.Time interval to run each test RX/TX. Default to
#H                 20 seconds.
#H    -l           Optional.Root name for logfiles to write to.
#H    -d --doc     Optional.Documentation.
#H    -h --help    Optional.Help information.
#H

#D  COPYRIGHT: Riventek
#D  LICENSE:   GPLv2
#D  AUTHOR:    franky@riventek.com
#D
#D  REFERENCES
#D    [1] http://www.slashroot.in/iperf-how-test-network-speedperformancebandwidth
#D    [2] https://openmaniak.com/iperf.php
#D    [3] http://blog.packagecloud.io/eng/2016/06/22/monitoring-tuning-linux-networking-stack-receiving-data/
#D

#########################################################################
# GENERAL SETUP
#########################################################################   
# Exit on error. Append || true if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case any command in a pipe fails. e.g. mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

#########################################################################
# VARIABLES SETUP 
#########################################################################
readonly RK_SCRIPT=$0              # Store the script name for help() and doc() functions
readonly RK_HAS_MANDATORY_ARGUMENTS="YES" # or "NO"
readonly TMPFILE=$(mktemp)      # Generate the temporary mask
# Add the commands and libraries required for the script to run
RK_DEPENDENCIES="ethtool sed grep date sort head tail cut tr"    
RK_LIBRARIES="lib_network.sh"

# Getting information about the interface used on default route (default)
interface=$(route | grep -m 1 default | tr -s ' ' | cut -f 8 -d ' ')
phy_interface=$(echo ${interface}| cut -f1 -d'.')
interface_speed=$(sudo ethtool ${phy_interface} | grep Speed  | cut -f2 -d':' | cut -f1 -d'b')
# Get the number of CPUs/Cores available.
let ncpus=$(( $( cat /proc/cpuinfo | grep -c processor) ))
# Get the Actual Governor
actual_governor=$(cpufreq-info -p 2> /dev/null | cut -f3 -d' ')
# Default values for optional arguments
udp=""
protocol="TCP"
bidir="-r"
interval_sec="20"
log_report_name=""

#########################################################################
# BASIC FUNCTIONS FOR ALL SCRIPTS
#########################################################################
# Function to extract the help usage from the script
help () {
	grep '^[ ]*[\#]*H' ${RK_SCRIPT} | sed 's/^[ ]*[\#]*H//g' | sed 's/^  //'
}
# Function to extract the documentation from the script
doc () {
  grep '^[ ]*[\#][\#]*[HDF]' ${RK_SCRIPT} | sed 's/^[ ]*[\#]*F / \>/g;s/^[ ]*[\#]*[HDF]//g' | sed 's/^  //'
}
# Function to print the errors and warnings
echoerr() {
  echo -e ${RK_SCRIPT}" [$(date +'%Y-%m-%d %H:%M:%S')] $@" >&2
}
# Function to check availability and load the required librarys
check_libraries() {
  if [[ ${RK_LIBRARIES:-} != "" ]]; then
	  for library in ${RK_LIBRARIES:-}; do
	    local missing=0
	    if [[ -r ${library} ]]; then
		    source ${library}
	    else
		    echoerr "> Required library  not found: ${library}"
		    let missing+=1
	    fi
	    if [[ ${missing} -gt 0 ]]; then
		    echoerr "** ERROR **: Cannot found ${missing} required libraries, aborting\n"
		    exit 1
	    fi
	  done
  fi
}
# Function to check if the required dependencies are available
check_dependencies() {
  local missing=0
  if [[ ${RK_DEPENDENCIES:-} != "" ]]; then
	  for command in ${RK_DEPENDENCIES}; do
	    if ! hash "${command}" >/dev/null 2>&1; then
		    echoerr "> Required Command not found in PATH: ${command}"
		    let missing+=1
	    fi
	  done
	  if [[ ${missing} -gt 0 ]]; then
	    echoerr "** ERROR **: Cannot found ${missing} required commands are missing in PATH, aborting\n"
	    exit 1
	  fi
  fi
}

#########################################################################
# MAIN SCRIPT
#########################################################################
# Check & Load required libraries
check_libraries
# Check if we have all the required commands
check_dependencies

# Command Line Parsing
echo -e "\n"
if [[ "${1:-}" == "" ]] && [[ ${RK_HAS_MANDATORY_ARGUMENTS} = "YES" ]]; then
  help
  exit 1
else
  while [[ "${1:-}" != "" ]]
  do
	  case ${1:-} in
      -c)
		    shift
		    iperf_server=${1:-}
		    echo "* Setting iPerf server to ${iperf_server} !"
		    shift
		    ;;
      -l)
		    shift
        if [[ "${1:-}" != "" ]]; then
          if ! $(touch "${1:-}"); then
            echoerr "** ERROR ** : Cannot create the report file in ${1:-} !"
            exit 1
          else
            log_report_name=${1:-};
            rm -f "${1:-}"
          fi
        fi
        shift
        ;;
      -t)
		    shift
		    if [[ $(( ${1:-} )) -gt 0 ]]; then
          interval_sec=$(( ${1:-} ))
		      echo "* Setting time interval to ${interval_sec} seconds !"
        else
          echoerr "** ERROR **: Time interval has to be greater than 0 seconds !"
          exit 1
        fi
		    shift
		    ;;
      -u)
        udp="-u"
        echo "* Setting UDP mode !"
        protocol="UDP"
        shift
		    ;;
      -b)
        bidir="-d"
        echo "* Setting Simultaneous BiDirectional Test mode !"
        shift
		    ;;
      -d|--doc)  
		    shift
		    doc
		    exit 0
		    ;;
      -h|--help)
		    shift
		    help
		    exit 0
		    ;;
      *)
        echoerr "** ERROR ** : Unsupported option '${1:-}' !!!!" 
		    help
		    exit 1
		    ;;
	  esac
  done
fi

# Try to ensure we are at maximum frequency
echo -e "\n>> Try to set the CPU Frequency at maximum ...\c"
sudo cpufreq-set -g performance 2> /dev/null
if [[ "$?" != "0" ]]; then
  echo " CANNOT !"
  echoerr "** WARNING **: Could not set the maximum frequency !"
else
  echo "OK !"
fi

#D  UDP TEST
#D    UDP will give invaluable information about the jitter and the packet loss.
#D    To keep a good link quality, the packet loss should not go over 1 %. A
#D    high packet loss rate will generate a lot of TCP segment retransmissions
#D    which will affect the bandwidth.
#D    Iperf doesn't have the necessary logic to find the maximum UDP transfer
#D    rate. We will need to increase the rate so long as all packets were
#D    getting through and back off when packets started dropping to find the
#D    maximum rate.
#D    So to select the appropriate bandwidth setting, we will need to do a first
#D    guess iteratively and check when we are under 1% packet loss. We will do
#D    it with multiple threads as is the most stressfull one.
#D
# if [[ ${protocol} = "UDP" ]]; then
#   echo -e ">> Finding a good bandwidth setting for ${protocol} (less than 1% error) "
  
#   # First estimate is the interface speed ( as we it's for CPU we divide the total by the number of CPUs )
#   let speed=$(( $( echo ${interface_speed} | grep -o [0-9]*[KMG] | cut -f1 -d'.' | grep -o "[0-9]*" ) / ${ncpus} ))
#   unit=$(echo ${interface_speed} | grep -o "[KMGkmg]" | tr '[:lower:]' '[:upper:]' )
#   udp_speed=${speed}${unit}

#   # Test the first estimate
#   iperf -c ${iperf_server} -u  -P ${ncpus} -t 10 ${bidir} -b ${udp_speed} > ${TMPFILE}-iperf-${ncpus}thread-test.tmp
#   report_iperf_info ${TMPFILE}-iperf-${ncpus}thread-test.tmp ${TMPFILE}-iperf-${ncpus}thread-test-report.tmp
  
#   # Now setup the starting speed as the minimum one in the test
#   udp_speed=$(grep "\[SUM\]" ${TMPFILE}-iperf-${ncpus}thread-test-report.tmp | grep -o '[0-9\.]*[ ]* [GMK]bits/sec' \
#                 | sort -n | head -n 1 | tr -d ' ' | cut -f1 -d 'b')
#   let speed=$(( $(echo ${udp_speed} | cut -f1 -d'.' | grep -o "[0-9]*" ) / ${ncpus} )) # Divided by CPU number
#   unit=$(echo ${udp_speed} | grep -o "[KMGkmg]" | tr '[:lower:]' '[:upper:]' )
#   udp_speed=${speed}${unit}
#   max_error=$(grep -o "[0-9e+\.]*%" ${TMPFILE}-iperf-${ncpus}thread-test.tmp | sed 's/e+02/00/' \
#                     | sed 's/%//' | sort -n -r | head -n1 | cut -f1 -d'.')
#   echo -e "\t+ UDP bandwidth=${udp_speed} => Maximum error=${max_error}% "

#   # Loop to refine the initial bandwidth
#   while [[ "${max_error}" != "0" ]] && [[ ${speed} -gt 1 ]]
#   do
#     let speed=$(( ${speed}*90/100 ))  # Reduce 10% the actual speed
#     if [[ "${speed}" = "0" ]];then
#       if [[ "${unit}" = "G" ]]; then
#         let speed=900
#         unit="M"
#       else
#         let speed=1
#       fi
#     fi
#     udp_speed=${speed}${unit}         
#     echo -e "\t+ UDP bandwidth=${udp_speed} \c"
#     iperf -c ${iperf_server} -u -P ${ncpus} -t 10 ${bidir} -b ${udp_speed} > ${TMPFILE}-iperf-${ncpus}thread-test.tmp
#     max_error=$(grep -o "[0-9e+\.]*%" ${TMPFILE}-iperf-${ncpus}thread-test.tmp | sed 's/e+02/00/' \
#                           | sed 's/%//' | sort -n -r | head -n1 | cut -f1 -d'.')
#     echo -e "=> Maximum error=${max_error}%"
#   done
#   echo ">> Setting UDP bandwidth to ${udp_speed} !"
#   udp=$udp" -b ${udp_speed}"
# fi

#D  SINGLE VS MULTIPLE THREADS
#D    We are going to make the tests with one single thread and also using all
#D    the CPUs available in order to stress the usage and distribution of the
#D    load across CPUs.
#D
#D  IRQs
#D    When a data frame is written to RAM via DMA, a NIC would generate an
#D    interrupt request (IRQ) indicating data had arrived. A device generating
#D    an IRQ when data has been written to RAM via DMA is simple enough, but if
#D    large numbers of data frames arrive this can lead to a large number of
#D    IRQs being generated. The more IRQs that are generated, the less CPU time
#D    is available for higher level tasks like user processes.
#D
#D    The CPU that handles the interrupt will be the CPU that processes the
#D    packet. In this way, arriving packets can be processed by separate CPUs
#D    from the hardware interrupt level up through the networking stack.
#D
#D    IRQ Balance is a service which can automatically balance interrupts across
#D    CPU cores, based on real time system conditions. However, manually
#D    balancing interrupts can be used to determine if `irqbalance` is not
#D    balancing IRQs in a optimum manner and therefore causing packet loss.
#D    There may be some very specific cases where manually balancing interrupts
#D    permanently can be beneficial. For this case, the interrupts will be
#D    manually associated with a CPU using SMP affinity.
#D
#D  SoftIRQs
#D    The softirq system in the Linux kernel is a mechanism for executing code
#D    outside of the context of an interrupt handler implemented in a driver.
#D    This system is important because hardware interrupts may be disabled
#D    during all or part of the execution of an interrupt handler. The longer
#D    interrupts are disabled, the greater chance that events may be missed.
#D
#D    The softirq system can be imagined as a series of kernel threads (one per
#D    CPU) that run handler functions which have been registered for different
#D    softirq events.
#D
#D    If the SoftIRQs do not run for long enough, the rate of incoming data
#D    could exceed the kernel's capability to drain the buffer fast enough. As a
#D    result, the NIC buffers will overflow and traffic will be lost.
#D
#D  CPU AND NETWORK DATA PROCESSING
#D    While the driver's IRQ handler itself does very little work itself, the
#D    softirq handler will execute on the same CPU as the driver's IRQ handler.
#D    This why setting the CPU a particular IRQ will be handled by is important:
#D    that CPU will be used not only to execute the interrupt handler in the
#D    driver, but the same CPU will also be used when harvesting packets in a
#D    softirq via NAPI. Things like Receive Packet Steering can distribute some
#D    of this work to other CPUs further up the network stack.
#D

## Single Thread Statistics
echo -e "\n** SINGLE THREAD STATISTICS **"
echo -e ">> Testing ${protocol} RX/TX independently with 1 Thread"
# Get the IRQ info before transmission
grep -E "${phy_interface}" /proc/interrupts | tr -s ' ' | cut -f3-$((3+${ncpus}-1)) -d' ' > ${TMPFILE}-irq-1thread-before.tmp
# Get the SoftIRQ info before transmission
grep -E "CPU|NET" /proc/softirqs |sed 's/^[ ]*NET/NET/' |tr -s ' ' | tr ' ' '\t' > ${TMPFILE}-softirq-1thread-before.tmp

# Thread to capture CPU information
{
  mpstat -u -P ALL 1 $(( $interval_sec + 10 )) > ${TMPFILE}-mpstat-1thread.tmp
}&
# Thread to test the ethernet performance with iperf
{
  iperf -c ${iperf_server} ${udp} -P 1 -t ${interval_sec} ${bidir} > ${TMPFILE}-iperf-1thread.tmp
}&
wait
# Get the IRQ info after transmission
grep -E "${phy_interface}" /proc/interrupts | tr -s ' ' | cut -f3-$((3+${ncpus}-1)) -d' ' > ${TMPFILE}-irq-1thread-after.tmp
# Get the SoftIRQ info after transmission
grep -E "CPU|NET" /proc/softirqs |sed 's/^[ ]*NET/NET/' |tr -s ' ' | tr ' ' '\t' > ${TMPFILE}-softirq-1thread-after.tmp

# Calculate the CPU vs IRQ/SoftIRQ differences
irqdiff="IRQ\t"
softirqTXdiff="SIRQ_TX\t"
softirqRXdiff="SIRQ_RX\t"
cpu="Diff\t"
for (( i=1; i<=${ncpus} ; i++)); do
  cpu=${cpu}"CPU$((${i}-1))\t"
  irqdiff=${irqdiff}$(cat ${TMPFILE}-irq-1thread-after.tmp ${TMPFILE}-irq-1thread-before.tmp | \
                        awk -v j=$i 'FNR == 1 {f1=$j} ; FNR == 2 {f2=$j} {print f1-f2}' | tail -n1)"\t"
  softirqTXdiff=${softirqTXdiff}$(grep TX ${TMPFILE}-softirq-1thread-after.tmp ${TMPFILE}-softirq-1thread-before.tmp | \
                                    cut -f3 -d':' | tr '\t' ' ' |awk -v j=$i 'FNR == 1 {f1=$j} ; FNR == 2 {f2=$j} {print f1-f2}' | tail -n1)"\t"
  softirqRXdiff=${softirqRXdiff}$(grep RX ${TMPFILE}-softirq-1thread-after.tmp ${TMPFILE}-softirq-1thread-before.tmp | \
                                    cut -f3 -d':' | tr '\t' ' ' |awk -v j=$i 'FNR == 1 {f1=$j} ; FNR == 2 {f2=$j} {print f1-f2}' | tail -n1)"\t"
done

# Reporting
if [[ "${log_report_name}" == "" ]]; then
  report_iperf_info ${TMPFILE}-iperf-1thread.tmp
  report_mpstat_info ${TMPFILE}-mpstat-1thread.tmp
  echo -e "\n** CPU vs IRQ delta distribution with 1 Thread **"
  echo -e "${cpu}\n${irqdiff}\n${softirqTXdiff}\n${softirqRXdiff}"
else
  echo -e "\n>> Writing iPerf report for 1 Thread on ${log_report_name}-iperf-${protocol}-1thread.log ..."
  report_iperf_info ${TMPFILE}-iperf-1thread.tmp ${log_report_name}-iperf-${protocol}-1thread.log
  echo -e "\n>> Writing MPSTAT report for 1 Thread on ${log_report_name}-mpstat-${protocol}-1thread.log ..."
  report_mpstat_info ${TMPFILE}-mpstat-1thread.tmp ${log_report_name}-mpstat-${protocol}-1thread.log
  echo -e "\n>> Writing CPU vs IRQ report for 1 Thread on ${log_report_name}-cpuvsirq-${protocol}-1thread.log ..."
  echo -e "${cpu}\n${irqdiff}\n${softirqTXdiff}\n${softirqRXdiff}" > ${log_report_name}-cpuvsirq-${protocol}-1thread.log
fi
## End Single Thread Statistics

## Multiple Thread Statitics
#echo "HACK - Press RETURN to continue ...";read # HACK to test with MAC OSX

# Only if more than 1 CPU
if [ ${ncpus} -gt 1 ]; then
  echo -e "\n** ${ncpus} THREADS STATISTICS **"
  echo -e ">> Testing ${protocol} TX/RX independently with ${ncpus} Threads"
  # Get the IRQ info before transmission
  grep -E "${phy_interface}" /proc/interrupts | tr -s ' ' | cut -f3-$((3+${ncpus}-1)) -d' ' > ${TMPFILE}-irq-${ncpus}thread-before.tmp
  # Get the SoftIRQ info before transmission
  grep -E "CPU|NET" /proc/softirqs |sed 's/^[ ]*NET/NET/' |tr -s ' ' | tr ' ' '\t' > ${TMPFILE}-softirq-${ncpus}thread-before.tmp

  # Thread to capture CPU information
  {
    mpstat -u -P ALL 1 $(( $interval_sec + 10 )) > ${TMPFILE}-mpstat-${ncpus}thread.tmp
  }&
  # Thread to test the ethernet performance with iperf
  {
    iperf -c ${iperf_server} ${udp} -P ${ncpus} -t ${interval_sec} ${bidir} > ${TMPFILE}-iperf-${ncpus}thread.tmp
  }&
  wait
  # Get the IRQ info after transmission
  grep -E "${phy_interface}" /proc/interrupts | tr -s ' ' | cut -f3-$((3+${ncpus}-1)) -d' ' > ${TMPFILE}-irq-${ncpus}thread-after.tmp
  # Get the SoftIRQ info after transmission
  grep -E "CPU|NET" /proc/softirqs |sed 's/^[ ]*NET/NET/' |tr -s ' ' | tr ' ' '\t' > ${TMPFILE}-softirq-${ncpus}thread-after.tmp

  # Calculate the CPU vs IRQ/SoftIRQ differences
  irqdiff="IRQ\t"
  softirqTXdiff="SIRQ_TX\t"
  softirqRXdiff="SIRQ_RX\t"
  cpu="Diff\t"
  for (( i=1; i<=${ncpus} ; i++)); do
    cpu=${cpu}"CPU$((${i}-1))\t"
    irqdiff=${irqdiff}$(cat ${TMPFILE}-irq-${ncpus}thread-after.tmp ${TMPFILE}-irq-${ncpus}thread-before.tmp | \
                          awk -v j=$i 'FNR == 1 {f1=$j} ; FNR == 2 {f2=$j} {print f1-f2}' | tail -n1)"\t"
    softirqTXdiff=${softirqTXdiff}$(grep TX ${TMPFILE}-softirq-${ncpus}thread-after.tmp ${TMPFILE}-softirq-${ncpus}thread-before.tmp | \
                                      cut -f3 -d':' | tr '\t' ' ' |awk -v j=$i 'FNR == 1 {f1=$j} ; FNR == 2 {f2=$j} {print f1-f2}' | tail -n1)"\t"
    softirqRXdiff=${softirqRXdiff}$(grep RX ${TMPFILE}-softirq-${ncpus}thread-after.tmp ${TMPFILE}-softirq-${ncpus}thread-before.tmp | \
                                      cut -f3 -d':' | tr '\t' ' ' |awk -v j=$i 'FNR == 1 {f1=$j} ; FNR == 2 {f2=$j} {print f1-f2}' | tail -n1)"\t"
  done
  
  # Reporting
  if [[ "${log_report_name}" == "" ]]; then
    report_iperf_info ${TMPFILE}-iperf-${ncpus}thread.tmp
    report_mpstat_info ${TMPFILE}-mpstat-${ncpus}thread.tmp
    echo -e "\n** CPU vs IRQ delta distribution with ${ncpus} Threads **"
    echo -e "${cpu}\n${irqdiff}\n${softirqTXdiff}\n${softirqRXdiff}"
  else
    echo -e "\n>> Writing iPerf report for ${ncpus} Threads on ${log_report_name}-iperf-${protocol}-${ncpus}thread.log ..."
    report_iperf_info ${TMPFILE}-iperf-1thread.tmp ${log_report_name}-${ncpus}perf-${protocol}-${ncpus}thread.log
    echo -e "\n>> Writing MPSTAT report for ${ncpus} Threads on ${log_report_name}-mpstat-${protocol}-${ncpus}thread.log ..."
    report_mpstat_info ${TMPFILE}-mpstat-1thread.tmp ${log_report_name}-mpstat-${protocol}-${ncpus}thread.log
    echo -e "\n>> Writing CPU vs IRQ report for ${ncpus} Threads on ${log_report_name}-cpuvsirq-${protocol}-${ncpus}thread.log ..."
    echo -e "${cpu}\n${irqdiff}\n${softirqTXdiff}\n${softirqRXdiff}" > ${log_report_name}-cpuvsirq-${protocol}-${ncpus}thread.log
  fi
fi
## End Multiple Thread Statitics

#D  MONITORING NETWORK DATA PROCESSING
#D    The network statistics are output to a file in proc: `/proc/net/softnet_stat`
#D    Important details about /proc/net/softnet_stat :
#D    - Each line of /proc/net/softnet_stat corresponds to a struct
#D      `softnet_data` structure, of which there is 1 per CPU.
#D    - The first value, `total/sec`, is the number of network frames processed.
#D      This can be more than the total number of network frames received if you
#D      are using ethernet bonding. There are cases where the ethernet bonding
#D      driver will trigger network data to be re-processed, which would
#D      increment the `total/sec` count more than once for the same packet.
#D    - The second value, `dropped/sec`, is the number of network frames dropped
#D      because there was no room on the processing queue.
#D    - The third value, `squeezed/sec`, is the number of times the
#D      `net_rx_action` loop terminated because the budget was consumed or the
#D      time limit was reached, but more work could have been done.
#D    - The next 5 values are always 0.
#D    - The ninth value, `collision/sec`, is a count of the number of times a
#D      collision occurred when trying to obtain a device lock when transmitting
#D      packets.
#D    - The tenth value, `rx_rps/sec`, is a count of the number of times this
#D      CPU has been woken up to process packets via an Inter-processor Interrupt
#D    - The last value, `flow_limit`, is a count of the number of times the
#D      flow limit has been reached.
#D

# Final statistics checking
echo -e "\n** NETWORK STATISTICS  **"
if [[ "${log_report_name}" == "" ]]; then
  echo -e "\ttotal/sec\tdropped/sec\tsqueezed/sec\tcollision/sec\trx_rps/sec\tflow_limit/sec"
else
  echo -e "\ttotal/sec\tdropped/sec\tsqueezed/sec\tcollision/sec\trx_rps/sec\tflow_limit/sec" > ${log_report_name}-softnet_stat-${protocol}.log
fi

for (( i=0; i<${ncpus} ; i++)); do
  line="CPU${i}\t"
  for j in 1 2 3 9 10 11
  do
    line=${line}$(( 16#$(awk -v jj=$j -v ii=$i 'FNR == ii {print $jj}' /proc/net/softnet_stat) ))"\t\t"
  done
  if [[ "${log_report_name}" == "" ]]; then
    echo -e ${line}
  else
    echo -e ${line} >> ${log_report_name}-softnet_stat-${protocol}.log
  fi
done

# Final clean up
echo -e "\n"
if [[ ${TMPFILE:-} != "" ]]; then
  rm -f ${TMPFILE:-}*
fi
