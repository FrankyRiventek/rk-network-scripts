#H
#H  lib_network.sh
#H
#H  DESCRIPTION
#H    This is a library that contains some functions to test network performance in linux systems.
#H    We are using the tool iperf2 rather than iperf3 because seems that iperf3 still has some issues[2]
#H    and also because iperf2 allows to make a TX/RX test in a single process, which optimizes the test.
#H    It also uses cpu-freq to state the CPU speed and mpstat to evaluate CPU impact
#H
#H  USAGE
#H    source lib_network.sh
#H
#H  ARGUMENTS
#H    -d --doc     Optional. Documentation.
#H    -h --help    Optional. Help information.
#H

#D  COPYRIGHT: Riventek
#D  LICENSE:   GPLv2
#D  AUTHOR:    franky@riventek.com
#D
#D  REFERENCES:
#D    [1] https://sandilands.info/sgordon/teaching/its323y15s1/iperf
#D    [2] https://fasterdata.es.net/performance-testing/network-troubleshooting-tools/iperf-and-iperf3/
#D    [3] https://www.es.net/assets/Uploads/201007-JTIperf.pdf
#D    [4] https://openmaniak.com/iperf.php
#D

# Only if the library is executed
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  #########################################################################
  # GENERAL SETUP
  #########################################################################   
  # Exit on error. Append || true if you expect an error.
  set -o errexit
  # Exit on error inside any functions or subshells.
  set -o errtrace
  # Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
  set -o nounset
  # Catch the error in case any command in a pipe fails. e.g. mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
  set -o pipefail
  # Turn on traces, useful while debugging but commented out by default
  # set -o xtrace
fi

#########################################################################
# VARIABLES SETUP 
#########################################################################
# Only if the library is executed
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  readonly RK_SCRIPT=$0              # Store the library name for help() and doc() functions
  readonly RK_HAS_MANDATORY_ARGUMENTS="YES" # In libraries should be always "YES" as only make sense sot execute them for documentation
else
  # Generate the temporary mask if needed
  if [[ ${TMPFILE:-} == "" ]]; then
	  readonly TMPFILE=$(mktemp)
  fi
fi
# Add the commands and libraries required for the script to run
RK_DEPENDENCIES=${RK_DEPENDENCIES:-}" sed grep date ethtool mpstat iperf cpufreq-set"    

#########################################################################
# BASIC FUNCTIONS FOR ALL SCRIPTS
#########################################################################
# Only if the library is executed
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  # Function to extract the help usage from the script
  help () {
	  grep '^[ ]*[\#]*H' ${RK_SCRIPT} | sed 's/^[ ]*[\#]*H//g' | sed 's/^  //'
  }
  # Function to extract the documentation from the script
  doc () {
	  grep '^[ ]*[\#][\#]*[HDF]' ${RK_SCRIPT} | sed 's/^[ ]*[\#]*F / \>/g;s/^[ ]*[\#]*[HDF]//g' | sed 's/^  //'
  }
  # Function to print the errors and warnings
  echoerr() {
    echo -e ${RK_SCRIPT}" [$(date +'%Y-%m-%d %H:%M:%S')] $@" >&2
  }
  # Function to check availability and load the required librarys
  check_libraries() {
	  if [[ ${RK_LIBRARIES:-} != "" ]]; then
	    for library in ${RK_LIBRARIES:-}; do
		    local missing=0
		    if [[ -r ${library} ]]; then
		      source ${library}
		    else
		      echoerr "> Required library  not found: ${library}"
		      let missing+=1
		    fi
		    if [[ ${missing} -gt 0 ]]; then
		      echoerr "** ERROR **: Cannot found ${missing} required libraries, aborting\n"
		      exit 1
		    fi
	    done
	  fi
  }
  # Function to check if the required dependencies are available
  check_dependencies() {
	  local missing=0
	  if [[ ${RK_DEPENDENCIES:-} != "" ]]; then
	    for command in ${RK_DEPENDENCIES}; do
		    if ! hash "${command}" >/dev/null 2>&1; then
		      echoerr "> Required Command not found in PATH: ${command}"
		      let missing+=1
		    fi
	    done
	    if [[ ${missing} -gt 0 ]]; then
		    echoerr "** ERROR **: Cannot found ${missing} required commands are missing in PATH, aborting\n"
		    exit 1
	    fi
	  fi
  }
fi

#D # MAIN LIBRARY FUNCTIONS #

#F
#F  FUNCTION:    parse_mpstat_cpu_info
#F  DESCRIPTION: This function will parse, select and generate a compact report out mpstat
#F  ARGUMENTS
#F    $1     File containing the MPSTAT log information 
report_mpstat_info()
{
  # CPU
  #     Processor number.The keyword all indicates that statistics are
  # calculated as averages among all processors.
  #
  # %usr
  #     Show  the  percentage  of CPU utilization that occurred while executing
  # at the user level (application).
  #
  # %sys
  #     Show  the  percentage  of CPU utilization that occurred while executing
  # at the system level (kernel). Note that this does not include time spent
  # servicing hardware and software interrupts.
  #
  # %irq
  #     Show the percentage of time spent by the CPU or CPUs to service hardware
  # interrupts.
  #
  # %soft
  #     Show the percentage of time spent by the CPU or CPUs to service software
  # interrupts.
  #
  # %idle
  #     Show the percentage of time that the CPU or CPUs were idle and the system
  # did not have an outstanding disk I/O request.

  local log_file
  local report_file
  local tmpfile
  tmpfile=${TMPFILE}-${FUNCNAME[0]}

  # Checking the parameters and copy to meaningful local variables.
  if [[ -r "${1:-}" ]]; then
    log_file="${1:-}"    
  else
	  echoerr "** ERROR in ${FUNCNAME[0]}() ** : Cannot find the MPSTAT log file in ${1:-} !"
    exit 1
  fi
  if [[ "${2:-}" != "" ]]; then
    report_file="${2:-}" 
    if ! $(touch ${report_file}); then
      echoerr "** ERROR in ${FUNCNAME[0]}() ** : Cannot create the report file in ${2:-} !"
      exit 1 
    fi
  else
    report_file=""
  fi
  
	echo -e "\n*** MPSTAT Info ***"  > ${tmpfile}-report.tmp
	cat "$1" | tr -s ' ' | grep "Average" | cut -f1,2,3,5,7,8,12 -d' ' | sed 's/Average: CPU/ CPU/'| sed 's/Average: /AvgCore:/' | tr ' ' '\t' >> ${tmpfile}-report.tmp 

  # Send the report to the adequate channel
  if [[ -z ${report_file} ]]; then
    cat ${tmpfile}-report.tmp
  else
    mv ${tmpfile}-report.tmp ${report_file}
  fi
  
  # Cleaning up 
  rm -Rf ${tmpfile}*
}

#F
#F  FUNCTION:    parse_iperf_info
#F  DESCRIPTION: This function will parse, select and generate a compact report out ipef test
#F  ARGUMENTS
#F    $1     File containing the iperf2 log information
#F    $2     Optional.File to write the report.
report_iperf_info()
{
  local log_file
  local report_file
  local number_threads
  local end_tx
  local start_rx
  local end_rx
  local server_start
  local local_start
  local total_kbytes
  local total_kbits
  local total_sum
  local tmpfile
  tmpfile=${TMPFILE}-${FUNCNAME[0]}

  # Checking the parameters and copy to meaningful local variables.
  if [[ -r "${1:-}" ]]; then
    log_file="${1:-}"    
  else
	  echoerr "** ERROR in ${FUNCNAME[0]}() ** : Cannot find the iPerf log file in ${1:-} !"
    exit 1
  fi
  if [[ "${2:-}" != "" ]]; then
    report_file="${2:-}" 
    if ! $(touch ${report_file}); then
      echoerr "** ERROR in ${FUNCNAME[0]}() ** : Cannot create the report file in ${2:-} !"
      exit 1 
    fi
  else
    report_file=""
  fi

  # Main function code
  
	echo -e "\n*** iPerf Report ***" > ${tmpfile}-report.tmp
	if [[ $(grep -c 'TCP' "${log_file}") -gt 0 ]]; then
    #
    # Processing TCP is simple and straigthforward :)
    #
    let number_threads=$(( $(grep -c 'local' "${log_file}" ) / 2 ))
	  let end_tx=${number_threads}+1
	  let start_rx=${end_tx}+1
	  let end_rx=${start_rx}+${number_threads}
	  # If we are processing TCP report
	  grep '\[[A-Z0-9 ]*\]' "${log_file}" | grep -v local | sed "2,${end_tx} s/\[[ 0-9]*\]/\[TX>\]/" \
      | sed "${start_rx},${end_rx} s/\[[ 0-9]*\]/\[RX<\]/"| sed 's/\[ ID\]/\>TCP\< /' >> ${tmpfile}-report.tmp
	else
    #
		# Processing a UDP is a bit more complex :|
    #
    # Some initial processing to check where Server Report starts
    grep '\[[A-Z0-9 ]*\]' "${log_file}" > ${tmpfile}-udp.tmp
    let server_start=$(( $( cat ${tmpfile}-udp.tmp | grep -n -m 1 "Server[ ]*Report" | cut -f1 -d':') ))
    let local_start=$(( $( cat ${tmpfile}-udp.tmp | tail -n +${server_start} | grep -n -m 1 "local" | cut -f1 -d':') ))
    
    # The header
    grep '\[[ ]*ID\]' ${tmpfile}-udp.tmp | sed 's/\[ ID\]/\>UDP\< /' >> ${tmpfile}-report.tmp
    # The TX version, just what the server has received
      cat ${tmpfile}-udp.tmp | tail -n +${server_start} | head -n $(( ${local_start} - 1)) | grep -v Server \
        | sed 's/\[[ 0-9]*\]/\[TX>\]/' >> ${tmpfile}-report.tmp

      # Generate the SUM line, if needed
      if [[ $(( $(grep -c '\[SUM\]' ${tmpfile}-udp.tmp ) )) -gt 0 ]]; then
        total_sum=$( cat ${tmpfile}-udp.tmp | grep -m 1 -o '\[SUM\][ ]*[0-9].[0-9]*\-[0-9]*.[0-9][ ]*[a-z]*'  \
                            | tr '\t' ' '| sed 's/\[SUM\][ ]*/\[SUM\]\__/')
        # Calculate the aggregated file size
        cat ${tmpfile}-udp.tmp | tail -n +${server_start} | head -n $(( ${local_start} - 1)) | grep -v Server \
          | sed 's/\[[ 0-9]*\]/\[TX>\]/' | grep -o '[0-9]*[\.]*[0-9]*[ ]* [GMK]Bytes'| tr -s ' ' > ${tmpfile}-udp-sum.tmp
        let total_kbytes=$( echo "scale=0;$(cat ${tmpfile}-udp-sum.tmp | sed 's/[ ]*GBytes/\*1024*1024/' \
                            | sed 's/[ ]*MBytes/\*1024/' | sed 's/[ ]*KBytes//' | tr '\n' '+' | sed 's/+$//' )" | bc | cut -f1 -d'.' )
        if  [[ $total_kbytes -gt $(( 1024*1024 )) ]]; then
          total_sum=${total_sum}"__$(echo "scale=1; ${total_kbytes}/1024/1024" | bc) GBytes"
        else
          total_sum=${total_sum}"___$(echo "scale=1; ${total_kbytes}/1024" | bc) MBytes"
        fi
        # Calculate the agregate bandwidth
        cat ${tmpfile}-udp.tmp | tail -n +${server_start} | head -n $(( ${local_start} - 1)) | grep -v Server \
          | sed 's/\[[ 0-9]*\]/\[TX>\]/' | grep -o '[0-9]*[\.]*[0-9]*[ ]* [GMK]bits/sec'| tr -s ' ' > ${tmpfile}-udp-sum.tmp
        let total_kbits=$( echo "scale=0;$(cat ${tmpfile}-udp-sum.tmp | sed 's/[ ]*Gbits\/sec/\*1000*1000/' \
                            | sed 's/[ ]*Mbits\/sec/\*1000/' | sed 's/[ ]*KBits\/sec//' | tr '\n' '+' | sed 's/+$//')" | bc | cut -f1 -d'.' )
        if  [[ $total_kbits -gt $(( 1000*1000 )) ]]; then
          total_sum=${total_sum}"__$(echo "scale=1; ${total_kbits}/1000/1000" | bc) Gbits/sec"
        else
          total_sum=${total_sum}"___$(echo "scale=1; ${total_kbits}/1000" | bc) Mbits/sec"
        fi
        echo ${total_sum} | sed 's/_/ /g' >> ${tmpfile}-report.tmp
      fi
      # The RX version, just what the is locally received
      cat ${tmpfile}-udp.tmp | tail -n +${server_start} | tail -n +${local_start} | grep -v local \
        | sed 's/\[[ 0-9]*\]/\[RX<\]/' >> ${tmpfile}-report.tmp
  fi
  # Send the report to the adequate channel
  if [[ ${report_file} = "" ]]; then
    cat ${tmpfile}-report.tmp
  else
    mv ${tmpfile}-report.tmp ${report_file}
  fi
  # Cleaning up 
  rm -Rf ${tmpfile}*  

}

#F
#F  FUNCTION:    function_name
#F  DESCRIPTION: This function is designed to do some of the amazing things
#F  GLOBALS:
#F    GLOBAL_VAR1  Variable and use of it
#F  ARGUMENTS:
#F    $1     Required important values 
#F    $2     Optional.
#F
function_name()
{
  local first_parameter
  local second_parameter
  local tmpfile
  tmpfile=${TMPFILE}-${FUNCNAME[0]}

  # Checking the parameters and copy to meaningful local variables.
  if [[ "${1:-}" != "" ]]; then
    first_parameter="${1:-}"    
  else
	  echoerr "** ERROR in ${FUNCNAME[0]}() ** : Cannot the find first parameter !"
    exit 1
  fi
  if [[ "${2:-}" != "" ]]; then
    second_parameter="${2:-}"
  else
    second_parameter=""
  fi

  # Main function code
  
  # Cleaning up 
  rm -Rf ${tmpfile}*  
}


# Only if the library is executed
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  #########################################################################
  # MAIN SCRIPT
  #########################################################################
  # Check & Load required libraries
  check_libraries
  # Check if we have all the required commands
  check_dependencies
  
  # Command Line Parsing
  if [[ "${1:-}" == "" ]] && [[ ${RK_HAS_MANDATORY_ARGUMENTS} = "YES" ]]; then
	  help
	  exit 1
  else
	  while [[ "${1:-}" != "" ]]
	  do
	    case "${1:-}" in
		    -d|--doc)  
		      shift
		      doc
		      exit 0
		      ;;
		    -h|--help)
		      shift
		      help
		      exit 0
		      ;;
		    *)
		      error "Option '$1' not supported !"
		      exit 1
		      ;;
	    esac
	  done
  fi
fi
